<?php

declare(strict_types=1);

namespace CoStack\ReversibleHaliteTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\ReversibleHalite\Operation\Encryption\AuthenticatedAsymmetricEncryption;
use ParagonIE\Halite\KeyFactory;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\ReversibleHalite\Operation\Encryption\AuthenticatedAsymmetricEncryption
 */
class AuthenticatedAsymmetricEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testAuthenticatedAsymmetricEncryptionCanBeReversed(): void
    {
        $senderKeyPair = KeyFactory::generateEncryptionKeyPair();
        $receiverKeyPair = KeyFactory::generateEncryptionKeyPair();

        $string = 'My super secret message';

        $senderReversible = new AuthenticatedAsymmetricEncryption(
            $senderKeyPair->getSecretKey(),
            $receiverKeyPair->getPublicKey()
        );

        $intermediate = $senderReversible->execute($string);

        $receiverReversible = new AuthenticatedAsymmetricEncryption(
            $senderKeyPair->getSecretKey(),
            $receiverKeyPair->getPublicKey()
        );

        $actual = $receiverReversible->reverse($intermediate);

        $this->assertSame($string, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testAuthenticatedAsymmetricEncryptionThrowsExceptionIfSomethingIsWrong(): void
    {
        $senderKeyPair = KeyFactory::generateEncryptionKeyPair();
        $receiverKeyPair = KeyFactory::generateEncryptionKeyPair();

        $string = 'My super secret message';

        $senderReversible = new AuthenticatedAsymmetricEncryption(
            $senderKeyPair->getSecretKey(),
            // Use a different key to sign than to validate the signature
            $senderKeyPair->getPublicKey()
        );

        $intermediate = $senderReversible->execute($string);

        $receiverReversible = new AuthenticatedAsymmetricEncryption(
            $senderKeyPair->getSecretKey(),
            $receiverKeyPair->getPublicKey()
        );

        $this->expectException(DecryptionFailedException::class);

        $receiverReversible->reverse($intermediate);
    }
}
