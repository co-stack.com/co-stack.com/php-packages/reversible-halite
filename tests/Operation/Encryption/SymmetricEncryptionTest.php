<?php

declare(strict_types=1);

namespace CoStack\ReversibleHaliteTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\ReversibleHalite\Operation\Encryption\SymmetricEncryption;
use ParagonIE\Halite\KeyFactory;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\ReversibleHalite\Operation\Encryption\SymmetricEncryption
 */
class SymmetricEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricEncryptionCanBeReversed(): void
    {
        $secret = KeyFactory::generateEncryptionKey();

        $symmetricEncryption = new SymmetricEncryption($secret);

        $string = 'My super secret string';

        $intermediate = $symmetricEncryption->execute($string);

        $this->assertStringNotContainsStringIgnoringCase($string, $intermediate);
        $this->assertStringNotContainsStringIgnoringCase($secret->getRawKeyMaterial(), $intermediate);

        $symmetricEncryption = new SymmetricEncryption($secret);

        $actual = $symmetricEncryption->reverse($intermediate);

        $this->assertSame($string, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException
     */
    public function testSymmetricEncryptionThrowsExceptionIfMessageIsMalformed(): void
    {
        $secret = KeyFactory::generateEncryptionKey();

        $symmetricEncryption = new SymmetricEncryption($secret);

        $this->expectException(DecryptionFailedException::class);

        $symmetricEncryption->reverse('invalid encrypted string');
    }
}
