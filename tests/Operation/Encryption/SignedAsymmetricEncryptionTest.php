<?php

declare(strict_types=1);

namespace CoStack\ReversibleHaliteTests\Operation\Encryption;

use CoStack\ReversibleHalite\Operation\Encryption\SignedAsymmetricEncryption;
use ParagonIE\Halite\KeyFactory;
use PHPUnit\Framework\TestCase;

/**
 * @covers \CoStack\ReversibleHalite\Operation\Encryption\SignedAsymmetricEncryption
 */
class SignedAsymmetricEncryptionTest extends TestCase
{
    public function testSignedAsymmetricEncryptionCanBeReversed(): void
    {
        $ourKeyPair = KeyFactory::generateSignatureKeyPair();
        $theirKeyPair = KeyFactory::generateSignatureKeyPair();

        $encryption = new SignedAsymmetricEncryption(
            $ourKeyPair->getSecretKey(),
            $theirKeyPair->getPublicKey()
        );

        $string = 'My super secret string';

        $intermediate = $encryption->execute($string);

        $encryption = new SignedAsymmetricEncryption(
            $theirKeyPair->getSecretKey(),
            $ourKeyPair->getPublicKey()
        );

        $actual = $encryption->reverse($intermediate);

        $this->assertSame($string, $actual);
    }
}
