<?php

declare(strict_types=1);

namespace CoStack\ReversibleHaliteTests\Operation\Encryption;

use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Exception\MissingPrivateKeyException;
use CoStack\Reversible\Exception\MissingPublicKeyException;
use CoStack\ReversibleHalite\Operation\Encryption\AnonymousAsymmetricEncryption;
use ParagonIE\Halite\KeyFactory;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \CoStack\ReversibleHalite\Operation\Encryption\AnonymousAsymmetricEncryption
 */
class AnonymousAsymmetricEncryptionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testAnonymousAsymmetricEncryptionCanBeReversed(): void
    {
        $receiverKeyPair = KeyFactory::generateEncryptionKeyPair();

        $string = 'My super secret message';

        $encryption = new AnonymousAsymmetricEncryption(
            null,
            $receiverKeyPair->getPublicKey()
        );

        $intermediate = $encryption->execute($string);

        $receiverReversible = new AnonymousAsymmetricEncryption(
            $receiverKeyPair->getSecretKey(),
            null
        );

        $actual = $receiverReversible->reverse($intermediate);

        $this->assertSame($string, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testAnonymousAsymmetricEncryptionThrowsExceptionIfThePublicKeyForEncryptionIsMissing(): void
    {
        $encryption = new AnonymousAsymmetricEncryption();

        $this->expectException(MissingPublicKeyException::class);

        $encryption->execute('foo');
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testAnonymousAsymmetricEncryptionThrowsExceptionIfThePrivateKeyForDecryptionIsMissing(): void
    {
        $encryption = new AnonymousAsymmetricEncryption();

        $this->expectException(MissingPrivateKeyException::class);

        $encryption->reverse('foo');
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     * @uses \CoStack\Reversible\Exception\DecryptionFailedException
     */
    public function testAnonymousAsymmetricEncryptionThrowsExceptionIfTheEncryptionKeyDoesNotMatchTheDecryptionKey(): void
    {
        $encryptionKeyPair = KeyFactory::generateEncryptionKeyPair();
        $decryptionKeyPair = KeyFactory::generateEncryptionKeyPair();

        $encryption = new AnonymousAsymmetricEncryption(null, $encryptionKeyPair->getPublicKey());
        $decryption = new AnonymousAsymmetricEncryption($decryptionKeyPair->getSecretKey());

        $intermediate = $encryption->execute('foo');

        $this->expectException(DecryptionFailedException::class);

        $decryption->reverse($intermediate);
    }
}
