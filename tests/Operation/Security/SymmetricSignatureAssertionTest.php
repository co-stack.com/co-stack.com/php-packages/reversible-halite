<?php

declare(strict_types=1);

namespace CoStack\ReversibleHaliteTests\Operation\Security;

use CoStack\Reversible\Exception\SignatureAssertionErrorException;
use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use CoStack\ReversibleHalite\Operation\Security\SymmetricSignatureAssertion;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Halite\KeyFactory;
use PHPUnit\Framework\TestCase;

use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;

/**
 * @coversDefaultClass \CoStack\ReversibleHalite\Operation\Security\SymmetricSignatureAssertion
 */
class SymmetricSignatureAssertionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionCanBeReversed(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        $actual = $assertion->reverse($intermediate);

        $this->assertSame($message, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageWasAltered(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        [$mac, $message] = json_decode_assoc($intermediate);
        $message .= ', not!';
        $intermediate = json_encode_strict([$mac, $message]);

        $this->expectException(SignatureAssertionFailedException::class);

        $assertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMacIsInvalid(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        [$mac, $message] = json_decode_assoc($intermediate);
        $mac .= ', not!';
        $intermediate = json_encode_strict([$mac, $message]);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMacWasAltered(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        [$mac, $message] = json_decode_assoc($intermediate);
        $mac = Base64UrlSafe::encode(random_bytes(SODIUM_CRYPTO_GENERICHASH_BYTES_MAX));
        $intermediate = json_encode_strict([$mac, $message]);

        $this->expectException(SignatureAssertionFailedException::class);

        $assertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageIsNoValidJson(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $malformedInput = 'foo';

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageDoesNotContainTwoElements(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $malformedInput = json_encode_strict('foo');

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionTestDoesNotThrowErrorIfJsonArrayHasStringKeys(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $malformedInput = json_encode_strict(['foo' => 'bar', 'baz' => 'bend']);

        try {
            $assertion->reverse($malformedInput);
        } catch (SignatureAssertionErrorException $exception) {
            // Ingored
        }

        $this->assertTrue(true);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageDoesNotContainThreeElements(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $malformedInput = json_encode_strict(['foo', 'bar', 'baz']);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageDoesNotContainTwoStrings(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $malformedInput = json_encode_strict([123, 'foo']);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageIsNotEncoded(): void
    {
        $secret = KeyFactory::generateAuthenticationKey();

        $assertion = new SymmetricSignatureAssertion($secret);

        $malformedInput = json_encode_strict(['bar', 'foo']);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }
}
