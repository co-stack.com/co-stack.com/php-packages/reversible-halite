<?php

declare(strict_types=1);

namespace CoStack\ReversibleHaliteTests\Operation\Security;

use CoStack\Reversible\Exception\MissingPrivateKeyException;
use CoStack\Reversible\Exception\MissingPublicKeyException;
use CoStack\Reversible\Exception\SignatureAssertionErrorException;
use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use CoStack\ReversibleHalite\Operation\Security\AsymmetricSignatureAssertion;
use ParagonIE\ConstantTime\Base64UrlSafe;
use ParagonIE\Halite\KeyFactory;
use PHPUnit\Framework\TestCase;

use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;

/**
 * @coversDefaultClass \CoStack\ReversibleHalite\Operation\Security\AsymmetricSignatureAssertion
 */
class AsymmetricSignatureAssertionTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testAsymmetricSignatureAssertionCanBeReversed(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $string = 'My super secret string';

        $intermediate = $assertion->execute($string);

        $actual = $assertion->reverse($intermediate);

        $this->assertSame($string, $actual);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageWasAltered(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        [$mac, $message] = json_decode_assoc($intermediate);
        $message .= ', not!';
        $intermediate = json_encode_strict([$mac, $message]);

        $this->expectException(SignatureAssertionFailedException::class);

        $assertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMacIsInvalid(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        [$mac, $message] = json_decode_assoc($intermediate);
        $mac .= ', not!';
        $intermediate = json_encode_strict([$mac, $message]);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMacWasAltered(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $message = 'This message must not be altered';

        $intermediate = $assertion->execute($message);

        [$mac, $message] = json_decode_assoc($intermediate);
        $mac = Base64UrlSafe::encode(random_bytes(SODIUM_CRYPTO_GENERICHASH_BYTES_MAX));
        $intermediate = json_encode_strict([$mac, $message]);

        $this->expectException(SignatureAssertionFailedException::class);

        $assertion->reverse($intermediate);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageIsNoValidJson(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $malformedInput = 'foo';

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageDoesNotContainTwoElements(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $malformedInput = json_encode_strict('foo');

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageDoesNotContainThreeElements(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $malformedInput = json_encode_strict(['foo', 'bar', 'baz']);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testSymmetricSignatureAssertionThrowsExceptionIfTheMessageDoesNotContainTwoStrings(): void
    {
        $signatureKeyPair = KeyFactory::generateSignatureKeyPair();

        $assertion = new AsymmetricSignatureAssertion(
            $signatureKeyPair->getPublicKey(),
            $signatureKeyPair->getSecretKey()
        );

        $malformedInput = json_encode_strict([123, 'foo']);

        $this->expectException(SignatureAssertionErrorException::class);

        $assertion->reverse($malformedInput);
    }

    /**
     * @covers ::__construct
     * @covers ::execute
     * @covers ::getExecutionClosure
     */
    public function testAsymmetricSignatureAssertionThrowsExceptionIsThePublicKeyForEncryptionIsMissing(): void
    {
        $assertion = new AsymmetricSignatureAssertion();

        $this->expectException(MissingPrivateKeyException::class);

        $assertion->execute('foo');
    }

    /**
     * @covers ::__construct
     * @covers ::reverse
     * @covers ::getReversionClosure
     */
    public function testAsymmetricSignatureAssertionThrowsExceptionIsThePrivateKeyForDecryptionIsMissing(): void
    {
        $assertion = new AsymmetricSignatureAssertion();

        $this->expectException(MissingPublicKeyException::class);

        $assertion->reverse('foo');
    }
}
