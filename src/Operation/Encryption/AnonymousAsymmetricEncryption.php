<?php

declare(strict_types=1);

namespace CoStack\ReversibleHalite\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;
use CoStack\Reversible\Exception\MissingPrivateKeyException;
use CoStack\Reversible\Exception\MissingPublicKeyException;
use ParagonIE\Halite\Alerts\InvalidKey;
use ParagonIE\Halite\Asymmetric\Crypto;
use ParagonIE\Halite\Asymmetric\EncryptionPublicKey;
use ParagonIE\Halite\Asymmetric\EncryptionSecretKey;
use ParagonIE\HiddenString\HiddenString;

class AnonymousAsymmetricEncryption extends AbstractReversible
{
    /** @var null|EncryptionSecretKey */
    private $ourPrivateKey;

    /** @var null|EncryptionPublicKey */
    private $theirPublicKey;

    public function __construct(EncryptionSecretKey $ourPrivateKey = null, EncryptionPublicKey $theirPublicKey = null)
    {
        if (null !== $ourPrivateKey) {
            $this->ourPrivateKey = $ourPrivateKey;
        }
        if (null !== $theirPublicKey) {
            $this->theirPublicKey = $theirPublicKey;
        }
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $input): string {
            if (null === $this->theirPublicKey) {
                throw new MissingPublicKeyException();
            }
            return Crypto::seal(new HiddenString($input), $this->theirPublicKey);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $input): string {
            if (null === $this->ourPrivateKey) {
                throw new MissingPrivateKeyException();
            }
            try {
                return Crypto::unseal($input, $this->ourPrivateKey)->getString();
            } catch (InvalidKey $exception) {
                throw new DecryptionFailedException([$exception->getMessage()]);
            }
        };
    }
}
