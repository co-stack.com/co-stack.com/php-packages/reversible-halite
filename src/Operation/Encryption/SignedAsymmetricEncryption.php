<?php

declare(strict_types=1);

namespace CoStack\ReversibleHalite\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use ParagonIE\Halite\Asymmetric\Crypto;
use ParagonIE\Halite\Asymmetric\SignaturePublicKey;
use ParagonIE\Halite\Asymmetric\SignatureSecretKey;
use ParagonIE\HiddenString\HiddenString;

class SignedAsymmetricEncryption extends AbstractReversible
{
    /** @var SignatureSecretKey */
    private $ourPrivateKey;

    /** @var SignaturePublicKey */
    private $theirPublicKey;

    public function __construct(SignatureSecretKey $ourPrivateKey, SignaturePublicKey $theirPublicKey)
    {
        $this->ourPrivateKey = $ourPrivateKey;
        $this->theirPublicKey = $theirPublicKey;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $input): string {
            return Crypto::signAndEncrypt(new HiddenString($input), $this->ourPrivateKey, $this->theirPublicKey);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $input): string {
            return Crypto::verifyAndDecrypt($input, $this->theirPublicKey, $this->ourPrivateKey)->getString();
        };
    }
}
