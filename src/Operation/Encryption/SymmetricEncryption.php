<?php

declare(strict_types=1);

namespace CoStack\ReversibleHalite\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;
use ParagonIE\Halite\Alerts\InvalidMessage;
use ParagonIE\Halite\Symmetric\Crypto;
use ParagonIE\Halite\Symmetric\EncryptionKey;
use ParagonIE\HiddenString\HiddenString;

class SymmetricEncryption extends AbstractReversible
{
    /** @var EncryptionKey */
    private $encryptionKey;

    public function __construct(EncryptionKey $encryptionKey)
    {
        $this->encryptionKey = $encryptionKey;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $input): string {
            return Crypto::encrypt(new HiddenString($input), $this->encryptionKey);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $input): string {
            try {
                return Crypto::decrypt($input, $this->encryptionKey)->getString();
            } catch (InvalidMessage $exception) {
                throw new DecryptionFailedException([$exception->getMessage()], $exception);
            }
        };
    }
}
