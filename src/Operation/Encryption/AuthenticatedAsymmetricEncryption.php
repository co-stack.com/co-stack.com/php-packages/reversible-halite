<?php

declare(strict_types=1);

namespace CoStack\ReversibleHalite\Operation\Encryption;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\DecryptionFailedException;
use ParagonIE\Halite\Alerts\InvalidMessage;
use ParagonIE\Halite\Asymmetric\Crypto;
use ParagonIE\Halite\Asymmetric\EncryptionPublicKey;
use ParagonIE\Halite\Asymmetric\EncryptionSecretKey;
use ParagonIE\HiddenString\HiddenString;

class AuthenticatedAsymmetricEncryption extends AbstractReversible
{
    /** @var EncryptionSecretKey */
    private $ourPrivateKey;

    /** @var EncryptionPublicKey */
    private $theirPublicKey;

    public function __construct(EncryptionSecretKey $ourPrivateKey, EncryptionPublicKey $theirPublicKey)
    {
        $this->ourPrivateKey = $ourPrivateKey;
        $this->theirPublicKey = $theirPublicKey;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $input): string {
            return Crypto::encrypt(new HiddenString($input), $this->ourPrivateKey, $this->theirPublicKey);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $input): string {
            try {
                return Crypto::decrypt($input, $this->ourPrivateKey, $this->theirPublicKey)->getString();
            } catch (InvalidMessage $exception) {
                throw new DecryptionFailedException([$exception->getMessage()], $exception);
            }
        };
    }
}
