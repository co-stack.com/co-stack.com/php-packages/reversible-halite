<?php

declare(strict_types=1);

namespace CoStack\ReversibleHalite\Operation\Security;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Exception\MissingPrivateKeyException;
use CoStack\Reversible\Exception\MissingPublicKeyException;
use CoStack\Reversible\Exception\SignatureAssertionErrorException;
use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use ParagonIE\Halite\Asymmetric\Crypto;
use ParagonIE\Halite\Asymmetric\SignaturePublicKey;
use ParagonIE\Halite\Asymmetric\SignatureSecretKey;
use RangeException;

use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;

class AsymmetricSignatureAssertion extends AbstractReversible
{
    /** @var null|SignaturePublicKey */
    private $publicKey;

    /** @var null|SignatureSecretKey */
    private $privateKey;

    public function __construct(SignaturePublicKey $publicKey = null, SignatureSecretKey $privateKey = null)
    {
        if (null !== $publicKey) {
            $this->publicKey = $publicKey;
        }
        if (null !== $privateKey) {
            $this->privateKey = $privateKey;
        }
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $input): string {
            if (null === $this->privateKey) {
                throw new MissingPrivateKeyException();
            }
            $signature = Crypto::sign($input, $this->privateKey);
            return json_encode_strict([$signature, $input]);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $input): string {
            if (null === $this->publicKey) {
                throw new MissingPublicKeyException();
            }
            try {
                $decoded = json_decode_assoc($input);
            } catch (JsonDecodeException $exception) {
                throw new SignatureAssertionErrorException(['The message is invalid'], $exception);
            }
            if (!is_array($decoded) || 2 !== count($decoded)) {
                throw new SignatureAssertionErrorException(['The message is invalid']);
            }
            [$signature, $message] = $decoded;
            if (!is_string($signature) || !is_string($message)) {
                throw new SignatureAssertionErrorException(['The message is invalid']);
            }
            try {
                $verify = Crypto::verify($message, $this->publicKey, $signature);
            } catch (RangeException $exception) {
                throw new SignatureAssertionErrorException(['The signature is malformed'], $exception);
            }
            if (!$verify) {
                throw new SignatureAssertionFailedException();
            }
            return $message;
        };
    }
}
