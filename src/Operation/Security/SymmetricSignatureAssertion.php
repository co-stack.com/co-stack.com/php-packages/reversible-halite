<?php

declare(strict_types=1);

namespace CoStack\ReversibleHalite\Operation\Security;

use Closure;
use CoStack\Reversible\AbstractReversible;
use CoStack\Reversible\Exception\JsonDecodeException;
use CoStack\Reversible\Exception\SignatureAssertionErrorException;
use CoStack\Reversible\Exception\SignatureAssertionFailedException;
use ParagonIE\Halite\Alerts\InvalidSignature;
use ParagonIE\Halite\Symmetric\AuthenticationKey;
use ParagonIE\Halite\Symmetric\Crypto;
use RangeException;

use function CoStack\Reversible\json_decode_assoc;
use function CoStack\Reversible\json_encode_strict;

class SymmetricSignatureAssertion extends AbstractReversible
{
    /** @var AuthenticationKey */
    private $authenticationKey;

    public function __construct(AuthenticationKey $secret)
    {
        $this->authenticationKey = $secret;
    }

    public function getExecutionClosure(): Closure
    {
        return function (string $input): string {
            $mac = Crypto::authenticate($input, $this->authenticationKey);
            return json_encode_strict([$mac, $input]);
        };
    }

    public function getReversionClosure(): Closure
    {
        return function (string $input): string {
            try {
                $decoded = json_decode_assoc($input);
            } catch (JsonDecodeException $exception) {
                throw new SignatureAssertionErrorException(['The message is invalid'], $exception);
            }
            if (!is_array($decoded) || 2 !== count($decoded)) {
                throw new SignatureAssertionErrorException(['The message is invalid']);
            }
            [$mac, $message] = array_values($decoded);
            if (!is_string($mac) || !is_string($message)) {
                throw new SignatureAssertionErrorException(['The message is invalid']);
            }
            try {
                $verify = Crypto::verify($message, $this->authenticationKey, $mac);
            } catch (RangeException | InvalidSignature $exception) {
                throw new SignatureAssertionErrorException(['The signature is malformed'], $exception);
            }
            if (!$verify) {
                throw new SignatureAssertionFailedException();
            }
            return $message;
        };
    }
}
